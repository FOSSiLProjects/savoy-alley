<?php require('login.php');?>
<!DOCTYPE html>
<html>

<head>
  <title>Savoy Alley &ndash; Add Slide</title>
  <link rel="stylesheet" type="text/css" href="../css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/datepicker.js"></script>
  <script type="text/javascript" src="../js/timepicker.js"></script>
  <script type="text/javascript" src="../js/w3.js"></script>
  <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />

</head>

<body>
  <div id="pagewidth">
    <div id="header"><h1>Savoy Alley &ndash; Digital Signage</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Add Slide</h1>
        <p>Accepted formats include: JPG, JPEG, PNG, GIF, MP4</p>
        <p style = "color:#ff0000;">All fields are required.</p>
        <hr>

<form action="action-add-slide.php" method="post" enctype="multipart/form-data">
  Image Title:<br />
    <input type="text" name="img_title" placeholder="Name your slide" required /><br /><br />

  Start Date / Time:<br />
    <input id="start_dt" name="start_dt" class='datepicker' size='11' title='D-MMM-YYYY' placeholder="Use date picker -> " required />
    <input id="start_tm" name="start_tm" class='timepicker' size='5'  title='HH:MM' placeholder="HH:MM" required/><br /><br />

  End Date / Time:<br />
    <input id="end_dt" name="end_dt" class='datepicker' size='11' title='D-MMM-YYYY' placeholder="Use date picker ->" required />
    <input id="end_tm" name="end_tm" class='timepicker' size='5'  title='HH:MM' placeholder="HH:MM" required /><br /><br />

    Set Slide to Active/Inactive<br />
    <input type="radio" name="activeslide" value="active" checked="checked">Active<br />
    <input type="radio" name="activeslide" value="inactive">Inactive<br /><br />

    Select media for upload (Max size = 10 MB):
    <input type="file" name="fileToUpload" id="fileToUpload" required><br /><br />
    <input type="submit" value="Add Slide" name="submit">
</form>

</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
