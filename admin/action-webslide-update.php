<?php require('login.php');?>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/w3.js"></script>
  <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body>
  <div id="pagewidth">
    <div id="header"><h1>Savoy Alley - Web Slide Updated</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Web Slide Edit Complete</h1>
<?php

include '../creds.php';



$web_title = $_POST["web_title"];
$start_dt = $_POST["start_dt"];
$start_tm = $_POST["start_tm"];
$end_dt = $_POST["end_dt"];
$end_tm = $_POST["end_tm"];
$activeslide = $_POST["activeslide"];
$url = $_POST["url"];
$editid = $_POST["webslidenumber"];

// DEBUGGING
/*
echo $img_title.'<br />';
echo $start_dt.'<br />';
echo $start_tm.'<br />';
echo $end_dt.'<br />';
echo $end_tm.'<br />';
echo $activeslide.'<br />';
echo $editid.'<br />';
*/


// Converting dates to SQL standards

$date = new DateTime();
$newDisplayDate = $date->createFromFormat('j-M-Y', $start_dt);
$displaydate = $newDisplayDate->format('Y-m-d');
$newExpireDate = $date->createFromFormat('j-M-Y', $end_dt);
$expiredate = $newExpireDate->format('Y-m-d');

$time = new DateTime();
$newDisplayTime = $time->createFromFormat('G:i', $start_tm);
$displaytime = $newDisplayTime->format('H:i:s');
$newExpireTime = $time->createFromFormat('G:i', $end_tm);
$expiretime = $newExpireTime->format('H:i:s');

echo "Web slide displays on: " . $SlideUp = $displaydate . " " . $displaytime;
echo "<br /><br />";
echo "Web slide expires on: " . $SlideDown = $expiredate . " " . $expiretime;
echo "<br /><br />";
echo "URL: ".$url;

echo '<hr>';
echo '<a href="webslide-add.php">Add another web slide</a> | <a href="webslide-manage.php">Manage web slides</a>';

// Updating the database

mysqli_query($conn,"UPDATE Websites SET WebName = '$web_title' WHERE WebID = '$editid'");
mysqli_query($conn,"UPDATE Websites SET WebUp = '$SlideUp' WHERE WebID = '$editid'");
mysqli_query($conn,"UPDATE Websites set WebDown = '$SlideDown' WHERE WebID = '$editid'");
mysqli_query($conn,"UPDATE Websites set WebActive = '$activeslide' WHERE WebID = '$editid'");
mysqli_query($conn,"UPDATE Websites set WebURL = '$url' WHERE WebID = '$editid'");

// Close database connection

mysqli_close($conn);

?>

</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>
</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
