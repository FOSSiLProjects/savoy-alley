<?php
include '../creds.php';
?>
<head>
  <meta http-equiv="refresh" content="900"> <!-- Refreshes page every 15 minutes to check for new content -->
<style>
body {overflow:hidden; background-color: #000;}
.mySlides {display:none;max-width: 100%;max-height: 100vh;margin: auto;}
</style>
</head>

<body>

<?php
// Get the latest slides from the database
$result = mysqli_query($conn,"SELECT SlideFile FROM Slides WHERE NOW() BETWEEN SlideUp AND SlideDown AND SlideActive = 'active'");
$webslides = mysqli_query($conn,"SELECT WebURL from Websites WHERE NOW() BETWEEN WebUp and WebDown AND WebActive = 'active'");

while($row = mysqli_fetch_array($result))
{
  $row_file = $row['SlideFile'];
  $info = pathinfo($row_file);

  //if ($info["extension"] == "jpg" || $info["extension"] == "jpeg") {
  if ($info["extension"] == "jpg" || $info["extension"] == "png") {
  echo '<img src="' . $row_file . '" class="mySlides">';
}
  elseif ($info["extension"] == "mp4") {

    echo '<video class="mySlides" autoplay loop>';
    echo '<source src="'.$row_file.'" type="video/mp4">';
    echo '</video>';

}
}

while($row = mysqli_fetch_array($webslides))
{
    $row_url = $row['WebURL'];
    echo '<iframe class="mySlides" src="'.$row_url.'" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;"></iframe>';
}

?>

<!-- JavaScript to change images -->
<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(carousel, 10000); // Change slide every 10 seconds
}
</script>
</body>
