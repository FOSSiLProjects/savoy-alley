<?php require('login.php');?>
<!DOCTYPE html>
<html>

<head>
  <title>Savoy Alley - Administration</title>
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
  <script type="text/javascript" src="../js/w3.js"></script>

</head>

<body>
  <div id="pagewidth">
    <div id="header"><h1>Savoy Alley &ndash; Digital Signage</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol">
        <img src="../images/SavoyAlley.png" width=100 alt="Savoy Alley logo" style="float: left; padding: 15px 15px 0px 5px;">
        <h1>Savoy Alley &ndash; Administration</h1>
        <h4>Johnny's in the basement mixin' up the medicine.</h4>
        <p>Welcome to Savoy Alley! Select from the options below to manage your slides.</p>

        <h3 style="color:#07617D; font-variant:small-caps;">Slide Management</h3>
        <p>Add to and control the slides in your digital signage deck.</p>
        <ul>
          <li><a href="slide-add.php">Add Slide</a> - Add a new slide to the deck.</li>
          <li><a href="slide-manage.php">Manage Slides</a> - Edit, activate, or delete current slides.</li>
        </ul>

        <h3 style="color:#07617D; font-variant:small-caps;">Web Slide Management</h3>
        <p>Add to and control the slides in your digital signage deck.</p>
        <ul>
          <li><a href="webslide-add.php">Add Web Slide</a> - Add a new web slide to the deck.</li>
          <li><a href="webslide-manage.php">Manage Web Slides</a> - Edit, activate, or delete current web slides.</li>
        </ul>
  <hr>


</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
