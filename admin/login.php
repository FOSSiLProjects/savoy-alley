<?php
//password needs to be sha1 encrypted - current password = password
$password = '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8';

session_start();
if (!isset($_SESSION['loggedIn'])) {
    $_SESSION['loggedIn'] = false;
}

if (isset($_POST['password'])) {
    if (sha1($_POST['password']) == $password) {
        $_SESSION['loggedIn'] = true;
    } else {
        die ('Incorrect password');
    }
}

if (!$_SESSION['loggedIn']): ?>

<head>
  <title>Savoy Alley - Log In</title>
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />

</head>

<html>
  <head>
    <title>Savoy Alley - Admin Login</title>
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
  </head>
  <body>
    <div id="pagewidth">
      <div id="header"><h1>Savoy Alley &ndash; Low faff digital signage</h1></div>
      <div id="wrapper" class="clearfix">
        <div id="maincol"><h1>Savoy Alley Administration</h1>
    <p style="color:red; font-weight: bold;">Login required</p>
    <form method="post">
      Password: <input type="password" name="password" autofocus> <br /><br />
      <input type="submit" name="submit" value="Login">
    </form>
    
  <img src="../images/SavoyAlley.png" alt="Savoy Alley logo" style="display: block; margin-left: auto; margin-right: auto; width: 40%;">


</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>

<?php
exit();
endif;
?>
