<?php require('login.php');?>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/w3.js"></script>
  <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
</head>
<body>
  <div id="pagewidth">
    <div id="header"><h1>Savoy Alley &ndash; Edit Slide</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Slide Edit Complete</h1>
<?php

include 'creds.php';

$timestamp = date(YmdHis);
$target_dir = "../uploads/";
$target_file = $target_dir . $timestamp . '-' . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


$img_title = $_POST["img_title"];
$start_dt = $_POST["start_dt"];
$start_tm = $_POST["start_tm"];
$end_dt = $_POST["end_dt"];
$end_tm = $_POST["end_tm"];
$activeslide = $_POST["activeslide"];
$editid = $_POST["slidenumber"];

// DEBUGGING

//echo $img_title.'<br />';
//echo $start_dt.'<br />';
//echo $start_tm.'<br />';
//echo $end_dt.'<br />';
//echo $end_tm.'<br />';
//echo $activeslide.'<br />';
//echo $editid.'<br />';

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 10000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "mp4") {
    echo "Sorry, only JPG, JPEG, PNG, GIF, & MP4 files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      echo "<strong>Slide title: </strong>" . $img_title;
      echo "<br /><br />";
      echo "<strong>File uploaded:</strong> ". basename( $_FILES["fileToUpload"]["name"]). "<br /><br />";
        echo "<strong>Slide path:</strong> " . $target_file . "<br /><br />";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

// Converting dates to SQL standards

$date = new DateTime();
$newDisplayDate = $date->createFromFormat('j-M-Y', $start_dt);
$displaydate = $newDisplayDate->format('Y-m-d');
$newExpireDate = $date->createFromFormat('j-M-Y', $end_dt);
$expiredate = $newExpireDate->format('Y-m-d');

$time = new DateTime();
$newDisplayTime = $time->createFromFormat('G:i', $start_tm);
$displaytime = $newDisplayTime->format('H:i:s');
$newExpireTime = $time->createFromFormat('G:i', $end_tm);
$expiretime = $newExpireTime->format('H:i:s');

echo "<strong>Display starts:</strong> " . $SlideUp = $displaydate . " " . $displaytime;
echo "<br /><br />";
echo "<strong>Display expires:</strong> " . $SlideDown = $expiredate . " " . $expiretime;
echo "<br /><br />";
if($imageFileType != "mp4") {
echo '<img src="' .$target_file . '" width="250"><br /><br />';
} else {
  echo '<video width="250" autoplay loop><source src="' . $target_file . '" type="video/mp4"></video><br /><br />';
}

echo '<hr>';
echo '<a href="slide-add.php">Add another slide</a> | <a href="slide-manage.php">Manage slides</a>';

// Updating the database

mysqli_query($conn,"UPDATE Slides SET SlideName = '$img_title' WHERE SlideID = '$editid'");
mysqli_query($conn,"UPDATE Slides SET SlideUp = '$SlideUp' WHERE SlideID = '$editid'");
mysqli_query($conn,"UPDATE Slides set SlideDown = '$SlideDown' WHERE SlideID = '$editid'");
mysqli_query($conn,"UPDATE Slides set SlideActive = '$activeslide' WHERE SlideID = '$editid'");
mysqli_query($conn,"UPDATE Slides set SlideFile = '$target_file' WHERE SlideID = '$editid'");

// Close database connection

mysqli_close($conn);

?>

</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>
</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->
</body>
</html>
