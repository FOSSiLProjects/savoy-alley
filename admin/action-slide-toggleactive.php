<?php require('login.php');?>
<?php

include '../creds.php';

$toggleid = htmlspecialchars($_GET["id"]);

// Updating the database
$result = mysqli_query($conn,"SELECT SlideActive FROM Slides WHERE SlideID = '$toggleid'");

while($row = mysqli_fetch_array($result))
{
  $active = $row['SlideActive'];

  if($active != "active") {
    mysqli_query($conn,"UPDATE Slides SET SlideActive = 'active' WHERE SlideID = '$toggleid'");
  } else {
    mysqli_query($conn,"UPDATE Slides SET SlideActive = 'inactive' WHERE SlideID = '$toggleid'");
  }
}

// Close database connection

mysqli_close($conn);

header("Location: slide-manage.php"); // Slide deleted - return to management

?>
