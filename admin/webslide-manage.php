<?php require('login.php');?>
<html>

<head>
  <title>Savoy Alley - Manage Web Slides</title>
  <link rel="stylesheet" type="text/css" href="../css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/datepicker.js"></script>
  <script type="text/javascript" src="../js/timepicker.js"></script>
  <script type="text/javascript" src="../js/w3.js"></script>
  <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
  <style>
  #slides {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 90%;
  }

  #slides td, #slides th {
      border: 1px solid #ddd;
      padding: 8px;
  }

  #slides tr:nth-child(even){background-color: #f2f2f2;}

  #slides tr:hover {background-color: #ddd;}

  #slides th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #0000ff;
      color: white;
  }
  </style>
</head>

<body>
  <div id="pagewidth">
    <div id="header"><h2>Savoy Alley - Web Slide Management</h2></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Manage Web Slides</h1>

<?php include '../creds.php';?>

<table id="slides">
  <tr>
    <th><strong>Name</strong></th>
    <th><strong>Displays</strong></th>
    <th><strong>Expires</strong></th>
    <th><strong>URL</strong></th>
    <th><strong>Active</strong></th>
    <th><strong>Edit</strong></th>
    <th><strong>Delete</strong></th>
  </tr>

<?php

$result = mysqli_query($conn,"SELECT * FROM Websites ORDER BY WebName");

while($row = mysqli_fetch_array($result))
{

  $row_webslideID = $row['WebID'];
  $row_webname = $row['WebName'];
  $row_displays = $row['WebUp'];
  $row_expires = $row['WebDown'];
  $row_active = $row['WebActive'];
  $row_url = $row['WebURL'];

  echo '<tr>';
  echo '<td>' . $row_webname . '</td>';
  echo '<td>' . $row_displays . '</td>';
  echo '<td>' . $row_expires . '</td>';
  echo '<td><a href="'.$row_url.'">'. $row_url . '</a></td>';
  echo '<td><a href="action-webslide-toggleactive.php?id=' . $row_webslideID . '">'.$row_active.'</a></td>';
  echo '<td><a href="webslide-edit.php?id=' . $row_webslideID . '">Edit</a></td>';
  echo '<td><a href="action-webslide-delete.php?id=' . $row_webslideID . '" onclick="return confirm(\'Are you sure you want to delete this web slide?\');">Delete</a></td>';
  echo '</tr>';
}

mysqli_close($conn);
?>

</table>
</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->

</body>
</html>
