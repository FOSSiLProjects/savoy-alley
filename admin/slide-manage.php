<?php require('login.php');?>
<html>

<head>
  <title>Savoy Alley &ndash; Manage Slides</title>
  <link rel="stylesheet" type="text/css" href="../css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="../css/layout.css" />
  <script type="text/javascript" src="../js/datepicker.js"></script>
  <script type="text/javascript" src="../js/timepicker.js"></script>
  <script type="text/javascript" src="../js/w3.js"></script>
  <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
  <style>
  #slides {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 90%;
  }

  #slides td, #slides th {
      border: 1px solid #ddd;
      padding: 8px;
  }

  #slides tr:nth-child(even){background-color: #f2f2f2;}

  #slides tr:hover {background-color: #ddd;}

  #slides th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #0000ff;
      color: white;
  }
  </style>
</head>

<body>
  <div id="pagewidth">
    <div id="header"><h1>Savoy Alley &ndash; Digital Signage</h1></div>
    <div id="wrapper" class="clearfix">
      <div id="maincol"><h1>Manage Slides</h1>

<?php include '../creds.php';?>

<table id="slides">
  <tr>
    <th><strong>Name</strong></th>
    <th><strong>Displays</strong></th>
    <th><strong>Expires</strong></th>
    <th><strong>Active</strong></th>
    <th><strong>Slide</strong></th>
    <th><strong>Edit</strong></th>
    <th><strong>Delete</strong></th>
  </tr>

<!-- echo '<a href="page1.php?custID='.$row["custID"].'">'.$row["custID"].'</a>';?> -->

<?php

$result = mysqli_query($conn,"SELECT * FROM Slides");

while($row = mysqli_fetch_array($result))
{

  $row_id = $row['SlideID'];
  $row_name = $row['SlideName'];
  $row_up = $row['SlideUp'];
  $row_down = $row['SlideDown'];
  $row_active = $row['SlideActive'];
  $row_file = $row['SlideFile'];

  $checkMP4 = new SplFileInfo($row_file);
  $fileExt = ($checkMP4->getExtension());

  echo '<tr>';
  echo '<td>' . $row_name . '</td>';
  echo '<td>' . $row_up . "</td>";
  echo '<td>' . $row_down . "</td>";
  //echo '<td>' . $row_active . "</td>";
  echo '<td><a href="action-slide-toggleactive.php?id=' . $row_id . '">'.$row_active.'</a></td>';
  if($fileExt != "mp4") {
  echo '<td><img src="'. $row_file . '" width="100"</td>';
  } else {
  echo '<td><video width="100" autoplay loop><source src="../' . $row_file . '" type="video/mp4"></video></td>';
}
  echo '<td><a href="slide-edit.php?id=' . $row_id . '">Edit</a></td>';
  echo '<td><a href="action-slide-delete.php?id=' . $row_id . '" onclick="return confirm(\'Are you sure you want to delete this slide?\');">Delete</a></td>';
  echo '</tr>';
}

mysqli_close($conn);
?>

</table>
</div> <!-- End maincol -->

<div id="leftcol">
<p w3-include-html="admin-nav.html"></p>

<script>
w3.includeHTML();
</script>

</div> <!-- End leftcol -->

</div> <!-- End wrapper -->
</div> <!-- End pagewidth -->

</body>
</html>
