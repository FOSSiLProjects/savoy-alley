-- Adminer 4.8.1 MySQL 5.5.5-10.3.31-MariaDB-0ubuntu0.20.04.1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `Slides`;
CREATE TABLE `Slides` (
  `SlideID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SlideName` text NOT NULL,
  `SlideUp` datetime NOT NULL,
  `SlideDown` datetime NOT NULL,
  `SlideActive` tinytext NOT NULL,
  `SlideFile` text NOT NULL,
  PRIMARY KEY (`SlideID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `Websites`;
CREATE TABLE `Websites` (
  `WebID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `WebName` text NOT NULL,
  `WebUp` datetime NOT NULL,
  `WebDown` datetime NOT NULL,
  `WebActive` tinytext NOT NULL,
  `WebURL` text NOT NULL,
  PRIMARY KEY (`WebID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2021-09-15 18:02:41
