# Savoy Alley
## Low faff digital signage

*You better duck down the alley way*

*Lookin' for a new friend*

~Bob Dylan - Subterranean Homesick Blues

Savoy Alley is a browser based digital signage solutions for libraries, schools, small businesses, or anyplace where they want an easy to use system to promote events, products, and ideas. You can run it on almost anything with a modern web browser, an internet connection, and a connection to a monitor or screen. Raspberry Pis, small form factor PCs, repurposed hardware, an old laptop -- it doesn't matter. That means Savoy Alley is a low cost solution for your digital signage desires.

### Features

Savoy Alley can display images, videos, and websites in a continuously rotating deck. Slides and websites can be scheduled in advance to appear and disappear from the deck, which means you can have slides in place for upcoming events that are weeks out, or longer. The following formats are supported:

* JPG/JPEG
* PNG
* GIF (including animated GIF)
* MP4

You can also provide a link to a web page and display that as part of the deck. This is useful for updating viewers on study room availability, library-of-things materials, and other dynamically shifting content tracked by external websites.

### More to come

Savoy Alley is under active development, so more features and ideas are coming soon!